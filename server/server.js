const express = require('express')

const PORT = 5000

const app = express()

app.get('/', function (req, res) {
    res.send('Hello World')
  })

app.listen(PORT, () => {
    console.log(`server started on ${PORT}`)
})