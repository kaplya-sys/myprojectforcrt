import React, {useEffect, useRef, useState} from "react"

export const Users = (props) => {
    const [users, setUsers] = useState([])
    const[errorMessage, setErrorMessage] = useState('')
    const listRef = useRef()

    useEffect(() => {
        const getUsers = async () => {
            try {
                const res = await fetch(`https://jsonplaceholder.typicode.com/users?_limit=${props.count}`)
                if (!res.ok) {
                    throw new Error("Server error!")
                }
                const json = await res.json()
                setUsers(json)
            } catch (error) {
                setErrorMessage(error.message)
            }
        }
        getUsers()
    }, [props.count])


    return (
        <div>
            <h3>
                Function components
            </h3>
            <ol className="users-list" ref={listRef}>
                {!errorMessage ? users.map(user => {
                    const {id, name, email, phone} = user
                    return (
                    <li key={id}>
                        {name}
                        <ul>
                            <li>
                                Email: {email}
                            </li>
                            <li>
                                Phone: {phone}
                            </li>
                        </ul>
                    </li>
                )}) :
                    <div>
                        {errorMessage}
                    </div>
                }
            </ol>
        </div>
    )
}