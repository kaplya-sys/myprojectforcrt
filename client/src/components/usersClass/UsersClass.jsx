import {Component, createRef} from "react"

export class UsersClass extends Component {
    constructor(props) {
        super(props)
        this.state = {
            users: [],
            errorMessage: ''
        }
        this.listRef = createRef()
    }

    async componentDidMount() {
        try {
            const res = await fetch(`https://jsonplaceholder.typicode.com/users?_limit=${this.props.count}`)
            if (!res.ok) {
                throw new Error("Server error!")
            }
            const json = await res.json()
            this.setState({ users: json })
        } catch (error) {
            this.setState({errorMessage: error.message})
        }
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        if (prevState.users.length < this.state.users.length) {
            const list = this.listRef.current
            return list.scrollHeight - list.scrollTop
        }

        return null
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.count !== this.props.count) {
            this.componentDidMount(this.props.count)
        }
        if (snapshot !== null) {
            const list = this.listRef.current
            list.scrollTop = list.scrollHeight - snapshot
        }
    }
    

    render() {
        return (
            <div>
                <h3>
                    Class components
                </h3>

                <ol className="users-list" ref={this.listRef}>
                    {!this.state.errorMessage ? this.state.users.map(user => {
                        const {id, name, email, phone} = user
                        return(
                        <li key={id}>
                            {name}
                            <ul>
                                <li>
                                    Email: {email}
                                </li>
                                <li>
                                    Phone: {phone}
                                </li>
                            </ul>
                        </li>
                    )}):
                        <div>
                            {this.state.errorMessage}
                        </div>
                    }
                </ol>
            </div>
        )
    }
}