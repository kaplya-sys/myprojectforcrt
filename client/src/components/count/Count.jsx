import {Component} from "react"
import {UsersClass} from "../usersClass/UsersClass"
import {Users} from "../users/Users"
import './Count.css'


export class Count extends Component {
    constructor(props) {
        super(props)
        this.state = {
            count: 0,
            setCount: ''
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleReset = this.handleReset.bind(this)
        this.handleKeypress = this.handleKeypress.bind(this)
    }

    handleChange(event) {
        this.setState({setCount: event.target.value})
    }

    handleSubmit() {
        this.setState({
            count: this.state.setCount,
            setCount: ''
        })
    }

    handleReset() {
        this.setState({count: 0 })
    }

    handleKeypress(event) {
        if (event.key === "Enter") {
            this.handleSubmit()
        }
    }

    render() {
        return (
            <>
                <div className="container">
                    <UsersClass count={this.state.count}/>
                    <Users count={this.state.count}/>
                </div>
                <div className="input-form" >
                    <label
                        className="label-text"
                    >
                        Введите количество пользователей, не более 10
                    </label>
                    <input 
                        className="input-count" 
                        type="text" 
                        value={this.state.setCount} 
                        onChange={this.handleChange}
                        onKeyPress={this.handleKeypress}
                    />
                    <input 
                        className="input-submit" 
                        type="submit" 
                        value="Отправить" 
                        onClick={this.handleSubmit} 
                    />
                    <input 
                        className="input-reset" 
                        type="submit"
                        value="Сбросить"
                        onClick={this.handleReset}
                    />
                </div>
            </>
        )
    }
}
